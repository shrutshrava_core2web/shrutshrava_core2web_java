class LogicalOperators {
    public static void main(String[] args) {

        boolean firstValue = true;
        boolean secondValue = false;

 
        boolean andResult = firstValue && secondValue;
        System.out.println("Logical AND: " + firstValue + " && " + secondValue + " = " + andResult);

 
        boolean orResult = firstValue || secondValue;
        System.out.println("Logical OR: " + firstValue + " || " + secondValue + " = " + orResult);

        boolean notFirstValue = !firstValue;
        System.out.println("Logical NOT for the first value: !" + firstValue + " = " + notFirstValue);

        boolean notSecondValue = !secondValue;
        System.out.println("Logical NOT for the second value: !" + secondValue + " = " + notSecondValue);
    }
}
