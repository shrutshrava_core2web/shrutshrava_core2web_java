class IncrementOperations {
    public static void main(String[] args) {
        int x = 19;

        // Calculation for x++ + x++
        int result1 = x++ + x++;
        System.out.println("Initial value of x: 19");
        System.out.println("Result of x++ + x++: " + result1); // should output 39

        // Reset x to 19 for the second calculation
        x = 19;

        // Calculation for ++x + x++ + ++x
        int result2 = ++x + x++ + ++x;
        System.out.println("Result of ++x + x++ + ++x: " + result2); // should output 62
    }
}
