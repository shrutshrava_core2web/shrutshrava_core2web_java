class RelationalOperators {
    public static void main(String[] args) {
 
        int firstNumber = 7;
        int secondNumber = 5;

        System.out.println(firstNumber + " > " + secondNumber + " : " + (firstNumber > secondNumber));
        System.out.println(firstNumber + " < " + secondNumber + " : " + (firstNumber < secondNumber));
        System.out.println(firstNumber + " == " + secondNumber + " : " + (firstNumber == secondNumber));
        System.out.println(firstNumber + " != " + secondNumber + " : " + (firstNumber != secondNumber));
        System.out.println(firstNumber + " >= " + secondNumber + " : " + (firstNumber >= secondNumber));
        System.out.println(firstNumber + " <= " + secondNumber + " : " + (firstNumber <= secondNumber));
    }
}
