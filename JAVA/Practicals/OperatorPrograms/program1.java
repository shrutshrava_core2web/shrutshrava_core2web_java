class BasicArithmeticOperations {
    public static void main(String[] args) {

        double firstNumber = 10;
        double secondNumber = 5;


        double additionResult = firstNumber + secondNumber;
        System.out.println("Addition: " + firstNumber + " + " + secondNumber + " = " + additionResult);


        double subtractionResult = firstNumber - secondNumber;
        System.out.println("Subtraction: " + firstNumber + " - " + secondNumber + " = " + subtractionResult);

 
        double multiplicationResult = firstNumber * secondNumber;
        System.out.println("Multiplication: " + firstNumber + " * " + secondNumber + " = " + multiplicationResult);

 
        double divisionResult = firstNumber / secondNumber;
        System.out.println("Division: " + firstNumber + " / " + secondNumber + " = " + divisionResult);
    }
}
