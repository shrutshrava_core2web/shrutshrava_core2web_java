class ClickCounter {
    public static void main(String[] args) {
        int clickCount = 0;

        // Simulate button clicks
        clickCount = clickCount++ + clickCount++ + clickCount++ + clickCount++;

        System.out.println("Total clicks: " + clickCount);
    }
}
