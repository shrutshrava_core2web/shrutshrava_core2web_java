class Increment{
    public static void main(String[] args) {
        int x = 14;
        int y = 2;

        // Calculation for ++x + y++
        int result1 = ++x + y++;
        System.out.println("Result of ++x + y++: " + result1); // should output 17

        // Reset x and y for the second calculation
        x = 14;
        y = 2;

        // Calculation for x++ + ++y + ++x + ++x
        int result2 = x++ + ++y + ++x + ++x;
        System.out.println("Result of x++ + ++y + ++x + ++x: " + result2); // should output 50

        // Reset x and y for the third calculation
        x = 14;
        y = 2;

        // Calculation for y++ + ++x + ++x
        int result3 = y++ + ++x + ++x;
        System.out.println("Result of y++ + ++x + ++x: " + result3); // should output 33
    }
}
