//program to use various operators learned in operators topic


class OperatorsExample {
    public static void main(String[] args) {
        // Arithmetic operators
        int a = 10;
        int b = 5;
        System.out.println("Arithmetic Operators:");
        System.out.println("a + b = " + (a + b)); // Addition
        System.out.println("a - b = " + (a - b)); // Subtraction
        System.out.println("a * b = " + (a * b)); // Multiplication
        System.out.println("a / b = " + (a / b)); // Division
        System.out.println("a % b = " + (a % b)); // Modulus

        // Relational operators
        System.out.println("\nRelational Operators:");
        System.out.println("a > b : " + (a > b)); // Greater than
        System.out.println("a < b : " + (a < b)); // Less than
        System.out.println("a == b : " + (a == b)); // Equal to
        System.out.println("a != b : " + (a != b)); // Not equal to
        System.out.println("a >= b : " + (a >= b)); // Greater than or equal to
        System.out.println("a <= b : " + (a <= b)); // Less than or equal to

        // Logical operators
        boolean x = true;
        boolean y = false;
        System.out.println("\nLogical Operators:");
        System.out.println("x && y : " + (x && y)); // Logical AND
        System.out.println("x || y : " + (x || y)); // Logical OR
        System.out.println("!x : " + (!x)); // Logical NOT

        // Assignment operators
        System.out.println("\nAssignment Operators:");
        int c = 15;
        c += 5; // Equivalent to c = c + 5
        System.out.println("c += 5 : " + c);
        c -= 5; // Equivalent to c = c - 5
        System.out.println("c -= 5 : " + c);
        c *= 2; // Equivalent to c = c * 2
        System.out.println("c *= 2 : " + c);
        c /= 3; // Equivalent to c = c / 3
        System.out.println("c /= 3 : " + c);
        c %= 4; // Equivalent to c = c % 4
        System.out.println("c %= 4 : " + c);

        // Unary operators
        System.out.println("\nUnary Operators:");
        int d = 10;
        System.out.println("d++ : " + (d++)); // Post-increment (print then increment)
        System.out.println("++d : " + (++d)); // Pre-increment (increment then print)
        System.out.println("d-- : " + (d--)); // Post-decrement (print then decrement)
        System.out.println("--d : " + (--d)); // Pre-decrement (decrement then print)

        // Bitwise operators
        int e = 5; // Binary: 101
        int f = 3; // Binary: 011
        System.out.println("\nBitwise Operators:");
        System.out.println("e & f : " + (e & f)); // Bitwise AND
        System.out.println("e | f : " + (e | f)); // Bitwise OR
        System.out.println("e ^ f : " + (e ^ f)); // Bitwise XOR
        System.out.println("~e : " + (~e)); // Bitwise Complement
        System.out.println("e << 1 : " + (e << 1)); // Left shift by 1 (results in 1010 which is 10 in decimal)
        System.out.println("e >> 1 : " + (e >> 1)); // Right shift by 1 (results in 10 which is 2 in decimal)
    }
}
