class BitwiseOperations {
    public static void main(String[] args) {
 
        int firstNumber = 5;
        int secondNumber = 3;

 
        int andResult = firstNumber & secondNumber;
        System.out.println("Bitwise AND: " + firstNumber + " & " + secondNumber + " = " + andResult);

 
        int orResult = firstNumber | secondNumber;
        System.out.println("Bitwise OR: " + firstNumber + " | " + secondNumber + " = " + orResult);

 
        int xorResult = firstNumber ^ secondNumber;
        System.out.println("Bitwise XOR: " + firstNumber + " ^ " + secondNumber + " = " + xorResult);

 
        int leftShiftResult = firstNumber << 1;
        System.out.println("Left shift: " + firstNumber + " << 1 = " + leftShiftResult);

 
        int rightShiftResult = firstNumber >> 1;
        System.out.println("Right shift: " + firstNumber + " >> 1 = " + rightShiftResult);
    }
}
