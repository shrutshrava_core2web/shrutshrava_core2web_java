import java.util.Scanner;

 class StringConcatenation {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

      
        System.out.print("Input 1: ");
        String input1 = scanner.nextLine();

        System.out.print("Input 2: ");
        String input2 = scanner.nextLine();

      
        String result = input1 + input2;

      
        int length = result.length();

      
        System.out.println("Output: " + result);
        System.out.println("Length of String: " + length);

      
    }
}
