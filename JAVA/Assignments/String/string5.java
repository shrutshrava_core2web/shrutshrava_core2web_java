import java.util.Scanner;

 class StringEquality {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        // Input strings from user
        System.out.print("Input str1: ");
        String str1 = scanner.nextLine();

        System.out.print("Input str2: ");
        String str2 = scanner.nextLine();

        // Check if the strings are equal
        boolean isEqual = str1.equals(str2);

        // Output the result
        System.out.println("Output: " + isEqual);

        scanner.close();
    }
}
