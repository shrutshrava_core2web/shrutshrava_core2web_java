import java.util.Scanner;

 class ReplaceCharacters {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        // Input string from user
        System.out.print("Input str: ");
        String str = scanner.nextLine();

        // Replace 'a' with 'e'
        String replacedStr = str.replace('a', 'e');

        // Output the result
        System.out.println("Output str: \"" + replacedStr + "\"");

 
    }
}
