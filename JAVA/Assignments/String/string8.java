import java.util.Scanner;

 class RemoveSpaces {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        // Input string from user
        System.out.print("Input String: ");
        String input = scanner.nextLine();

        // Remove leading and trailing spaces
        String output = input.trim();

        // Output the result
        System.out.println("Output String: \"" + output + "\"");
    }
 }
