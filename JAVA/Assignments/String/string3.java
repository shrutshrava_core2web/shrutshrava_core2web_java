import java.util.Scanner;

 class PrintCharacters {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

 
        System.out.print("Input: ");
        String input = scanner.nextLine();

 
        System.out.println("Output:");
        for (int i = 0; i < input.length(); i++) {
            System.out.println(input.charAt(i));
        }

 
    }
}
