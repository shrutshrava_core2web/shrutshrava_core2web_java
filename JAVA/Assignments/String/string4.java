import java.util.Scanner;

class CompareStrings {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

 
        System.out.print("Input str1: ");
        String str1 = scanner.nextLine();

        System.out.print("Input str2: ");
        String str2 = scanner.nextLine();

 
        int differences = 0;
        int minLength = Math.min(str1.length(), str2.length());
        for (int i = 0; i < minLength; i++) {
            if (str1.charAt(i) != str2.charAt(i)) {
                differences += Math.abs(str1.charAt(i) - str2.charAt(i));
            }
        }

 
        differences += Math.abs(str1.length() - str2.length());

 
        if (differences == 0) {
            System.out.println("Difference = 0");
        } else {
            System.out.println("Difference = " + differences);
        }

 
    }
}
