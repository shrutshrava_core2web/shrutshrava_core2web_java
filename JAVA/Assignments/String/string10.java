import java.util.Scanner;

 class LastCharacter {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        // Input string from user
        System.out.print("Input string: ");
        String input = scanner.nextLine();

        // Check if the string is empty
        if (!input.isEmpty()) {
            // Get the last character of the string
            char lastChar = input.charAt(input.length() - 1);
            System.out.println("Last character: " + lastChar);
        } else {
            System.out.println("The string is empty.");
        }

     }
}
