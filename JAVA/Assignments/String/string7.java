import java.util.Scanner;

 class UppercaseConverter {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        // Input string from user
        System.out.print("Input: ");
        String input = scanner.nextLine();

        // Convert the string to uppercase
        String uppercaseString = input.toUpperCase();

        // Output the result
        System.out.println("Output: " + uppercaseString);

        
    }
}
