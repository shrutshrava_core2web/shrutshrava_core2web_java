class Student {
    public static void main(String[] args) {
        // Data types used: int, double, char, boolean, and String
        
        int studentId = 12345;
        
        
        String studentName = "John Doe";
        
        
        double studentGPA = 3.85;
        
        char gradeLevel = 'A';
        
        boolean isEnrolled = true;
        

        System.out.println("Student ID: " + studentId);
        System.out.println("Student Name: " + studentName);
        System.out.println("Student GPA: " + studentGPA);
        System.out.println("Grade Level: " + gradeLevel);
        System.out.println("Enrollment Status: " + (isEnrolled ? "Enrolled" : "Not Enrolled"));
    }
}
