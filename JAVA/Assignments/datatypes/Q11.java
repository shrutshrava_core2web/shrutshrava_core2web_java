class Gravity {
    public static void main(String[] args) {
 
        double gravity = 9.8; // Standard value of acceleration due to gravity
        
 
        char gravitySymbol = 'g';
        
 
        System.out.println("Value of gravity: " + gravity + " m/s^2");
        
 
        System.out.println("Symbol for acceleration due to gravity: " + gravitySymbol);
    }
}
