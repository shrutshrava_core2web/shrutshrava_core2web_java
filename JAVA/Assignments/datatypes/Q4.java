 class CompanyDetails {
    public static void main(String[] args) {
        // Employee count of the company
        int employeeCount = 500; // Example value
        
        // Total expenses of the company on travel in dollars
        double travelExpenses = 25000.0; // Example value
        
        // Print the employee count of the company
        System.out.println("Employee Count: " + employeeCount);
        
        // Print the total expenses of the company on travel
        System.out.println("Total Travel Expenses: $" + travelExpenses);
    }
}
