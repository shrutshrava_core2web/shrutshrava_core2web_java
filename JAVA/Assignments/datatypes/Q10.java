class MovieDetails {
    public static void main(String[] args) {
 
        double boxOfficeCollection = 500.75; // Example value
        
 
        double imdbRating = 8.4; // Example value
        
 
        System.out.println("Box Office Collection: $" + boxOfficeCollection + " million");
        
 
        System.out.println("IMDb Rating: " + imdbRating + "/10");
    }
}
