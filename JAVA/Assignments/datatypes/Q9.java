class Temperature {
    public static void main(String[] args) {
       
    	double airConditionerTemp = 22.5; // Example value
        
        
        double standardRoomTemp = 24.0; // Standard value
        
        
        System.out.println("Air Conditioner Temperature: " + airConditionerTemp + "°C");
        
        
        System.out.println("Standard Room Temperature: " + standardRoomTemp + "°C");
    }
}
