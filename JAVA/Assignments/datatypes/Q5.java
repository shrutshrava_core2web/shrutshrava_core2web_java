class KeyboardDetails {
    public static void main(String[] args) {
        // Count of buttons on the keyboard
        int buttonCount = 104; // Example value
        
        // Cost of the keyboard in dollars
        double keyboardCost = 49.99; // Example value
        
        // Print the count of buttons
        System.out.println("Button Count: " + buttonCount);
        
        // Print the cost of the keyboard
        System.out.println("Keyboard Cost: $" + keyboardCost);
    }
}
