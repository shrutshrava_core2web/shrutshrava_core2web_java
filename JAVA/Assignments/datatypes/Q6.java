class DateAndTime {
    public static void main(String[] args) {
        // Date, month, and year
        int date = 4;
        int month = 6;
        int year = 2024;
        
        
        System.out.println("Date: " + date);
        System.out.println("Month: " + month);
        System.out.println("Year: " + year);
        
        
        int secondsInMinute = 60;
        int minutesInHour = 60;
        int hoursInDay = 24;
        int daysInMonth = 30;
        int daysInYear = 365;        
        int totalSecondsInDay = secondsInMinute * minutesInHour * hoursInDay;
        int totalSecondsInMonth = totalSecondsInDay * daysInMonth;
        int totalSecondsInYear = totalSecondsInDay * daysInYear;
        
        
        System.out.println("Total seconds in a day: " + totalSecondsInDay);
        System.out.println("Total seconds in a month: " + totalSecondsInMonth);
        System.out.println("Total seconds in a year: " + totalSecondsInYear);
    }
}
