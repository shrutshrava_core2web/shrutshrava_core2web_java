



class CircleDetails {
    public static void main(String[] args) {
        // Value of pi
        double pi = 3.14159; // Approximate value

        // Radius of the circle
        double radius = 5.0; // Example value

        // Calculate the area of the circle using pi
        double area = pi * radius * radius;

        // Print the value of pi
        System.out.println("Value of Pi: " + pi);

        // Print the area of the circle
        System.out.println("Area of the Circle: " + area);

        // Print the highest degree of an angle of a circle
        System.out.println("Highest Degree of Angle of Circle: 360 degrees");
    }
}
