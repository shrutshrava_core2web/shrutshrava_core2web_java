class Population {
    public static void main(String[] args) {
      
        long worldPopulation = 7795000000L; // 7.795 billion (as of 2021)
        
      
        long indiaPopulation = 1393409038L; // 1.393 billion (as of 2021)
        
      
        System.out.println("World's Population: " + worldPopulation);
        
      
        System.out.println("India's Population: " + indiaPopulation);
    }
}
