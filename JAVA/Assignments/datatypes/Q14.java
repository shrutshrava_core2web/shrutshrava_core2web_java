class AsciiToChar {
    public static void main(String[] args) {
        
        int ascii1 = 67;
        int ascii2 = 79;
        int ascii3 = 82;
        int ascii4 = 69;
        int ascii5 = 50;
        int ascii6 = 87;
        int ascii7 = 69;
        int ascii8 = 66;
        
        
        char char1 = (char) ascii1;
        char char2 = (char) ascii2;
        char char3 = (char) ascii3;
        char char4 = (char) ascii4;
        char char5 = (char) ascii5;
        char char6 = (char) ascii6;
        char char7 = (char) ascii7;
        char char8 = (char) ascii8;
        
        System.out.print(char1);
        System.out.print(char2);
        System.out.print(char3);
        System.out.print(char4);
        System.out.print(char5);
        System.out.print(char6);
        System.out.print(char7);
        System.out.print(char8);
    }
}
