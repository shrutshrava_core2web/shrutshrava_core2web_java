class Main {
    public static void main(String[] args) {
        int[] array = {2, 5, 2, 7, 8, 9, 2};
        int specificNumber = 2;
        int count = 0;
        
        for (int num : array) {
            if (num == specificNumber) {
                count++;
            }
        }
        
        if (count > 0) {
            System.out.println("Number " + specificNumber + " occurred " + count + " times in the array.");
        } else {
            System.out.println("Number " + specificNumber + " not found in array.");
        }
    }
}
