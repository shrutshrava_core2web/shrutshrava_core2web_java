import java.util.Scanner;

 class FirstPrimeInArray {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter the size of the array:");
        int size = scanner.nextInt();
        int[] arr = new int[size];

        System.out.println("Enter the elements of the array:");
        for (int i = 0; i < size; i++) {
            arr[i] = scanner.nextInt();
        }

        int firstPrimeIndex = findFirstPrimeIndex(arr);
        
        if (firstPrimeIndex != -1) {
            System.out.println("First prime number found at index " + firstPrimeIndex);
        } else {
            System.out.println("No prime number found in the array.");
        }

        scanner.close();
    }

    private static int findFirstPrimeIndex(int[] arr) {
        for (int i = 0; i < arr.length; i++) {
            if (isPrime(arr[i])) {
                return i;
            }
        }
        return -1;
    }

    private static boolean isPrime(int num) {
        if (num <= 1) {
            return false;
        }
        for (int i = 2; i <= Math.sqrt(num); i++) {
            if (num % i == 0) {
                return false;
            }
        }
        return true;
    }
}
