class Main {
    public static void main(String[] args) {
        int[] array = {1, 4, 5, 6, 11, 9, 10};
        int product = 1;
        
        for (int num : array) {
            if (isPrime(num)) {
                product *= num;
            }
        }
        
        System.out.println(product);
    }

    public static boolean isPrime(int num) {
        if (num <= 1) {
            return false;
        }
        for (int i = 2; i <= Math.sqrt(num); i++) {
            if (num % i == 0) {
                return false;
            }
        }
        return true;
    }
}
