import java.util.Scanner;

 class CompositeNumbersInArray {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter the size of the array:");
        int size = scanner.nextInt();
        int[] arr = new int[size];

        System.out.println("Enter the elements of the array:");
        for (int i = 0; i < size; i++) {
            arr[i] = scanner.nextInt();
        }

        System.out.print("Composite numbers in an array are: ");
        boolean foundComposite = false;
        for (int i = 0; i < size; i++) {
            if (isComposite(arr[i])) {
                if (foundComposite) {
                    System.out.print(", ");
                }
                System.out.print(arr[i]);
                foundComposite = true;
            }
        }

        if (!foundComposite) {
            System.out.println("No composite numbers found in the array.");
        }

        scanner.close();
    }

    private static boolean isComposite(int num) {
        if (num <= 1) {
            return false;
        }
        for (int i = 2; i <= Math.sqrt(num); i++) {
            if (num % i == 0) {
                return true;
            }
        }
        return false;
    }
}
