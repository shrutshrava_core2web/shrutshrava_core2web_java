class Main {
    public static void main(String[] args) {
        int[] array1 = {121, 144, 225, 88, 90, 89};
        int[] array2 = {1, 625, 196, 169, 7};

        printNumbers(array1);
        printNumbers(array2);
    }

    public static void printNumbers(int[] array) {
        if (array.length % 2 != 0 && array.length >= 5) {
            for (int num : array) {
                if (num % 2 != 0) {
                    System.out.print(num + " ");
                }
            }
        } else {
            for (int num : array) {
                if (num % 2 == 0) {
                    System.out.print(num + " ");
                }
            }
        }
        System.out.println();
    }
}
