import java.util.Scanner;

 class SumOf2DArray {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter the number of rows:");
        int rows = scanner.nextInt();

        System.out.println("Enter the number of columns:");
        int columns = scanner.nextInt();

        int[][] array = new int[rows][columns];

        System.out.println("Enter the elements of the array:");

        // Input loop to populate the array
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                array[i][j] = scanner.nextInt();
            }
        }

        int sum = calculateSum(array);

        System.out.println("Sum = " + sum);

        scanner.close();
    }

    public static int calculateSum(int[][] array) {
        int sum = 0;

        // Sum loop to calculate the sum of the array
        for (int[] row : array) {
            for (int num : row) {
                sum += num;
            }
        }

        return sum;
    }
}
