import java.util.Scanner;

 class CountPalindromeElements {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter the size of the array:");
        int size = scanner.nextInt();
        int[] arr = new int[size];

        System.out.println("Enter the elements of the array:");
        for (int i = 0; i < size; i++) {
            arr[i] = scanner.nextInt();
        }

        int palindromeCount = 0;
        for (int i = 0; i < size; i++) {
            if (isPalindrome(arr[i])) {
                palindromeCount++;
            }
        }

        System.out.println("Count of palindrome elements is: " + palindromeCount);

        scanner.close();
    }

    public static boolean isPalindrome(int num) {
        int originalNum = num;
        int reversedNum = 0;

        while (num != 0) {
            int digit = num % 10;
            reversedNum = reversedNum * 10 + digit;
            num /= 10;
        }

        return originalNum == reversedNum;
    }
}
