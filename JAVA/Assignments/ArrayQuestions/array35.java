import java.util.Scanner;

 class CountDigitsInArray {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter the size of the array:");
        int size = scanner.nextInt();
        int[] arr = new int[size];

        System.out.println("Enter the elements of the array:");
        for (int i = 0; i < size; i++) {
            arr[i] = scanner.nextInt();
        }

        for (int i = 0; i < size; i++) {
            int count = countDigits(arr[i]);
            System.out.print(count);
            if (i != size - 1) {
                System.out.print(", ");
            }
        }

        scanner.close();
    }

    private static int countDigits(int num) {
        if (num == 0) {
            return 1; // 0 has 1 digit
        }

        int count = 0;
        while (num != 0) {
            num = num / 10;
            count++;
        }
        return count;
    }
}
