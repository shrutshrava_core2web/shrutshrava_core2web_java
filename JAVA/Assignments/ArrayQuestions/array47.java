import java.util.Scanner;

 class AsciiValueToChar {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter the size of the array:");
        int size = scanner.nextInt();
        Object[] arr = new Object[size];  // Use Object array to store both integers and characters

        System.out.println("Enter the elements of the array:");
        for (int i = 0; i < size; i++) {
            int element = scanner.nextInt();
            if (element >= 65 && element <= 90) {
                arr[i] = (char) element;  // Type cast to char if in ASCII range of 'A' to 'Z'
            } else {
                arr[i] = element;
            }
        }

        System.out.print("Array will be like: ");
        for (int i = 0; i < size; i++) {
            System.out.print(arr[i]);
            if (i != size - 1) {
                System.out.print(" ");
            }
        }

 
    }
}
