import java.util.Scanner;

 class SumOfSecondaryDiagonal {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter the number of rows:");
        int rows = scanner.nextInt();

        System.out.println("Enter the number of columns:");
        int columns = scanner.nextInt();

        int[][] array = new int[rows][columns];

        System.out.println("Enter the elements of the array:");

        // Input loop to populate the array
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                array[i][j] = scanner.nextInt();
            }
        }

        int sum = calculateSecondaryDiagonalSum(array);

        System.out.println("Sum of Secondary Diagonal : " + sum);

        scanner.close();
    }

    public static int calculateSecondaryDiagonalSum(int[][] array) {
        int sum = 0;
        int n = array.length; // Assuming square matrix

        for (int i = 0; i < n; i++) {
            sum += array[i][n - i - 1];
        }

        return sum;
    }
}
