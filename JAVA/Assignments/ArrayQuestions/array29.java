import java.util.Scanner;

 class IncrementDigitsToArray {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter the Number:");
        long number = scanner.nextLong();

        String numberString = Long.toString(number);
        int length = numberString.length();
        int[] resultArray = new int[length];

        for (int i = 0; i < length; i++) {
            int digit = Character.getNumericValue(numberString.charAt(i));
            resultArray[i] = digit + 1;
        }

        System.out.print("Output: ");
        for (int i = 0; i < length; i++) {
            System.out.print(resultArray[i]);
            if (i != length - 1) {
                System.out.print(", ");
            }
        }

        scanner.close();
    }
}
