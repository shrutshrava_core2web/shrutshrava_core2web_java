class Main {
    public static void main(String[] args) {
        int[] array = {1, 5, 9, 8, 7, 6};
        int specificNumber = 5;
        int index = -1;
        
        for (int i = 0; i < array.length; i++) {
            if (array[i] == specificNumber) {
                index = i;
                break;
            }
        }
        
        if (index != -1) {
            System.out.println("num " + specificNumber + " first occurred at index: " + index);
        } else {
            System.out.println("num " + specificNumber + " not found in array.");
        }
    }
}
