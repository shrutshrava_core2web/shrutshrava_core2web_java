import java.util.Scanner;

 class PalindromeArray {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter the size of the array:");
        int size = scanner.nextInt();
        int[] arr = new int[size];

        System.out.println("Enter the elements of the array:");
        for (int i = 0; i < size; i++) {
            arr[i] = scanner.nextInt();
        }

        boolean isPalindrome = true;
        for (int i = 0; i < size / 2; i++) {
            if (arr[i] != arr[size - i - 1]) {
                isPalindrome = false;
                break;
            }
        }

        if (isPalindrome) {
            System.out.println("The given array is a palindrome array.");
        } else {
            System.out.println("The given array is not a palindrome array.");
        }

        scanner.close();
    }
}
