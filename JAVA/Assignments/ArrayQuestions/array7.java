import java.util.Scanner;

 class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter size:");
        int size = scanner.nextInt();
        int[] array = new int[size];

        System.out.println("Enter elements:");
        for (int i = 0; i < size; i++) {
            array[i] = scanner.nextInt();
        }

        System.out.println("Output:");
        for (int i = 0; i < size; i++) {
            if (array[i] % 4 == 0) {
                System.out.println(array[i] + " is divisible by 4 and its index is " + i);
            }
        }

 
    }
}
