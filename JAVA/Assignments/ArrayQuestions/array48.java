import java.util.Scanner;

 class ReverseCharArray {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter the size of the array:");
        int size = scanner.nextInt();
        char[] arr = new char[size];

        System.out.println("Enter the elements of the array:");
        for (int i = 0; i < size; i++) {
            arr[i] = scanner.next().charAt(0);
        }

        // Print alternate elements before reverse
        System.out.print("Before Reverse: ");
        for (int i = 0; i < size; i += 2) {
            System.out.print(arr[i] + " ");
        }
        System.out.println();

        // Reverse the array
        reverseArray(arr);

        // Print alternate elements after reverse
        System.out.print("After Reverse: ");
        for (int i = 0; i < size; i += 2) {
            System.out.print(arr[i] + " ");
        }
        System.out.println();

        scanner.close();
    }

    public static void reverseArray(char[] arr) {
        int left = 0;
        int right = arr.length - 1;
        while (left < right) {
            char temp = arr[left];
            arr[left] = arr[right];
            arr[right] = temp;
            left++;
            right--;
        }
    }
}
