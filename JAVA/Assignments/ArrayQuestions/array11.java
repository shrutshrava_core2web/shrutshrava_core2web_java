class Main {
    public static void main(String[] args) {
        int[] array = {10, 20, 30, 40, 50, 60};
        
        // Add 15 to each element
        for (int i = 0; i < array.length; i++) {
            array[i] += 15;
        }
        
        // Print the updated array
        for (int num : array) {
            System.out.print(num + " ");
        }
    }
}
