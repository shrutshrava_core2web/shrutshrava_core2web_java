import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

 class CommonElementsInArrays {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter the size of the arrays:");
        int size = scanner.nextInt();

        int[] arr1 = new int[size];
        int[] arr2 = new int[size];

        System.out.println("Enter the elements of the first array:");
        for (int i = 0; i < size; i++) {
            arr1[i] = scanner.nextInt();
        }

        System.out.println("Enter the elements of the second array:");
        for (int i = 0; i < size; i++) {
            arr2[i] = scanner.nextInt();
        }

        Set<Integer> set1 = new HashSet<>();
        Set<Integer> commonElements = new HashSet<>();

        for (int num : arr1) {
            set1.add(num);
        }

        for (int num : arr2) {
            if (set1.contains(num)) {
                commonElements.add(num);
            }
        }

        System.out.print("Common elements in the given arrays are: ");
        if (commonElements.isEmpty()) {
            System.out.print("None");
        } else {
            boolean first = true;
            for (int num : commonElements) {
                if (!first) {
                    System.out.print(", ");
                }
                System.out.print(num);
                first = false;
            }
        }

 
    }
}
