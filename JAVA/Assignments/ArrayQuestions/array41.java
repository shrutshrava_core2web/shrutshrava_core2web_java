import java.util.Scanner;

 class DescendingOrderCheck {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter the size of the array:");
        int size = scanner.nextInt();
        int[] arr = new int[size];

        System.out.println("Enter the elements of the array:");
        for (int i = 0; i < size; i++) {
            arr[i] = scanner.nextInt();
        }

        boolean isDescending = true;
        for (int i = 1; i < size; i++) {
            if (arr[i] > arr[i - 1]) {
                isDescending = false;
                break;
            }
        }

        if (isDescending) {
            System.out.println("Given array is in descending order.");
        } else {
            System.out.println("Given array is not in descending order.");
        }

 
    }
}
