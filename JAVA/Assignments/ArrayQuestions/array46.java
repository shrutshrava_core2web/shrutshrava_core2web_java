import java.util.Scanner;

 class MultiplesInArray {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter the size of the array:");
        int size = scanner.nextInt();
        int[] arr = new int[size];

        System.out.println("Enter the elements of the array:");
        for (int i = 0; i < size; i++) {
            arr[i] = scanner.nextInt();
        }

        System.out.println("Enter key:");
        int key = scanner.nextInt();

        boolean found = false;
        for (int i = 0; i < size; i++) {
            if (arr[i] % key == 0) {
                System.out.println("An element multiple of " + key + " found at index: " + i);
                found = true;
            }
        }

        if (!found) {
            System.out.println("Element Not Found.");
        }

     }
}
