import java.util.Scanner;

 class SumOfOddRows2DArray {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter the number of rows:");
        int rows = scanner.nextInt();

        System.out.println("Enter the number of columns:");
        int columns = scanner.nextInt();

        int[][] array = new int[rows][columns];

        System.out.println("Enter the elements of the array:");

        // Input loop to populate the array
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                array[i][j] = scanner.nextInt();
            }
        }

        // Calculate and print the sum of odd rows
        for (int i = 0; i < rows; i += 2) {
            int sum = 0;
            for (int j = 0; j < columns; j++) {
                sum += array[i][j];
            }
            System.out.println("Sum of row " + (i + 1) + " = " + sum);
        }

 
    }
}
