import java.util.Scanner;

 class ReplaceWithCube {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter the size of the array:");
        int size = scanner.nextInt();
        int[] arr = new int[size];

        System.out.println("Enter the elements of the array:");
        for (int i = 0; i < size; i++) {
            arr[i] = scanner.nextInt();
        }

        System.out.println("Enter key:");
        int key = scanner.nextInt();

        int count = 0;
        for (int i = 0; i < size; i++) {
            if (arr[i] == key) {
                count++;
            }
        }

        if (count > 2) {
            for (int i = 0; i < size; i++) {
                if (arr[i] == key) {
                    arr[i] = (int) Math.pow(key, 3); // replace with cube
                }
            }

            System.out.print("Array will be like: ");
            for (int i = 0; i < size; i++) {
                System.out.print(arr[i]);
                if (i != size - 1) {
                    System.out.print(" ");
                }
            }
        } else {
            System.out.println("Element Not Found.");
        }

        scanner.close();
    }
}
