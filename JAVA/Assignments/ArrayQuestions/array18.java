class Main {
    public static void main(String[] args) {
        int[] array = {4, 5, 7, 9, 10};
        
        for (int num : array) {
            if (isComposite(num)) {
                System.out.print(num + " ");
            }
        }
    }

    public static boolean isComposite(int num) {
        if (num <= 1) {
            return false;
        }
        for (int i = 2; i <= num / 2; i++) {
            if (num % i == 0) {
                return true;
            }
        }
        return false;
    }
}
