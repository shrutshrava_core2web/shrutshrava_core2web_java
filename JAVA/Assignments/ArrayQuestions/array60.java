import java.util.Scanner;

 class CornerElements2DArray {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter the number of rows:");
        int rows = scanner.nextInt();

        System.out.println("Enter the number of columns:");
        int columns = scanner.nextInt();

        int[][] array = new int[rows][columns];

        System.out.println("Enter the elements of the array:");

        // Input loop to populate the array
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                array[i][j] = scanner.nextInt();
            }
        }

        // Print corner elements
        System.out.println("Output:");
        System.out.print(array[0][0] + " "); // Top-left
        System.out.print(array[0][columns - 1] + " "); // Top-right
        System.out.print(array[rows - 1][0] + " "); // Bottom-left
        System.out.println(array[rows - 1][columns - 1]); // Bottom-right

 
    }
}
