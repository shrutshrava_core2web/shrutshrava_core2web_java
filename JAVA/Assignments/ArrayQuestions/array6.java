import java.util.Scanner;

 class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter the size of the character array:");
        int size = scanner.nextInt();
        char[] charArray = new char[size];

        System.out.println("Enter characters:");
        for (int i = 0; i < size; i++) {
            charArray[i] = scanner.next().charAt(0);
        }

        System.out.println("Output:");
        for (int i = 0; i < size; i++) {
            System.out.print(charArray[i] + " ");
        }

    }
}
