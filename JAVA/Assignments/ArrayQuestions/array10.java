import java.util.Scanner;

 class ArrayExample {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter the number of students:");
        int numStudents = scanner.nextInt();

        // Create an array to store the grades
        int[] grades = new int[numStudents];

        // Input grades from the user
        System.out.println("Enter the grades of students:");
        for (int i = 0; i < numStudents; i++) {
            System.out.print("Student " + (i + 1) + ": ");
            grades[i] = scanner.nextInt();
        }

        // Calculate the average grade
        int sum = 0;
        for (int grade : grades) {
            sum += grade;
        }
        double average = (double) sum / numStudents;

        System.out.println("Average grade: " + average);

 
    }
}
