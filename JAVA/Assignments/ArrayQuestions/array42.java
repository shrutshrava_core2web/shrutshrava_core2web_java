import java.util.Scanner;

 class PrimeNumbersInArray {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter the size of the array:");
        int size = scanner.nextInt();
        int[] arr = new int[size];

        System.out.println("Enter the elements of the array:");
        for (int i = 0; i < size; i++) {
            arr[i] = scanner.nextInt();
        }

        int sumOfPrimes = 0;
        int countOfPrimes = 0;

        for (int i = 0; i < size; i++) {
            if (isPrime(arr[i])) {
                sumOfPrimes += arr[i];
                countOfPrimes++;
            }
        }

        System.out.println("Sum of all prime numbers is " + sumOfPrimes + " and count of prime numbers in the given array is " + countOfPrimes);

        scanner.close();
    }

    private static boolean isPrime(int num) {
        if (num <= 1) {
            return false;
        }
        for (int i = 2; i <= Math.sqrt(num); i++) {
            if (num % i == 0) {
                return false;
            }
        }
        return true;
    }
}
