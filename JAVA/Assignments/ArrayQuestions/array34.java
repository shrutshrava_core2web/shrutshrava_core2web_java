import java.util.*;

 class FirstDuplicate {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter the size of the array:");
        int size = scanner.nextInt();
        int[] arr = new int[size];

        System.out.println("Enter the elements of the array:");
        for (int i = 0; i < size; i++) {
            arr[i] = scanner.nextInt();
        }

        Map<Integer, Integer> map = new HashMap<>();
        int duplicateIndex = -1;

        for (int i = 0; i < size; i++) {
            if (map.containsKey(arr[i])) {
                duplicateIndex = map.get(arr[i]);
                break;
            } else {
                map.put(arr[i], i);
            }
        }

        if (duplicateIndex != -1) {
            System.out.println("First duplicate element present at index " + duplicateIndex);
        } else {
            System.out.println("No duplicate element found in the array.");
        }

        scanner.close();
    }
}
