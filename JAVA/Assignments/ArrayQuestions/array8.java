import java.util.Scanner;

 class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter the number of employees:");
        int count = scanner.nextInt();
        int[] ages = new int[count];

        System.out.println("Enter the ages of employees:");
        for (int i = 0; i < count; i++) {
            ages[i] = scanner.nextInt();
        }

        System.out.println("Employees' ages:");
        for (int i = 0; i < count; i++) {
            System.out.println("Employee " + (i + 1) + ": " + ages[i]);
        }

 
    }
}
