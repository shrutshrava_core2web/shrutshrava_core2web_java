class Main {
    public static void main(String[] args) {
        int[] array = {-2, 5, -6, 7, -3, 8};
        
        for (int i = 0; i < array.length; i++) {
            if (array[i] < 0) {
                array[i] = array[i] * array[i];
            }
        }
        
        for (int num : array) {
            System.out.print(num + " ");
        }
    }
}
