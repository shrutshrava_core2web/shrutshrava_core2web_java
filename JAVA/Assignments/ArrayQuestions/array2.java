import java.util.Scanner;

 class Main {
    public static void main(String[] args) {
        int[] array = new int[10];
        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter 10 elements:");

        for (int i = 0; i < 10; i++) {
            array[i] = scanner.nextInt();
        }

        System.out.print("Array: ");
        for (int i = 0; i < array.length; i++) {
            if (i > 0) {
                System.out.print(", ");
            }
            System.out.print(array[i]);
        }

 
    }
}
