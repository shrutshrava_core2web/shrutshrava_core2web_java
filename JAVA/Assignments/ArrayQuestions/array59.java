import java.util.Scanner;

 class ProductOfDiagonalSums {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter the number of rows:");
        int rows = scanner.nextInt();

        System.out.println("Enter the number of columns:");
        int columns = scanner.nextInt();

        int[][] array = new int[rows][columns];

        System.out.println("Enter the elements of the array:");

        // Input loop to populate the array
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                array[i][j] = scanner.nextInt();
            }
        }

        int primarySum = calculatePrimaryDiagonalSum(array);
        int secondarySum = calculateSecondaryDiagonalSum(array);

        int product = primarySum * secondarySum;

        System.out.println("Product of Sum of Primary and Secondary Diagonal : " + product);

        scanner.close();
    }

    public static int calculatePrimaryDiagonalSum(int[][] array) {
        int sum = 0;
        int n = array.length; // Assuming square matrix

        for (int i = 0; i < n; i++) {
            sum += array[i][i];
        }

        return sum;
    }

    public static int calculateSecondaryDiagonalSum(int[][] array) {
        int sum = 0;
        int n = array.length; // Assuming square matrix

        for (int i = 0; i < n; i++) {
            sum += array[i][n - i - 1];
        }

        return sum;
    }
}
